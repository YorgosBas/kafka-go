package main

import (
	"fmt"
	"os"
	"time"
	"github.com/Shopify/sarama"
)

func main() {
	fmt.Println("Starting Kafka producer...")

	//Producer configuration
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	config.Producer.Return.Errors = true
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5

	//Create new producer for given brokers and configuration
	brokers := []string{brokerAddr()}
	producer, err := sarama.NewSyncProducer(brokers, config)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := producer.Close(); err != nil {
			panic(err)
		}
	}()

	topic := topic()
	msgCount := 0

	//Get signal for finish
	doneCh := make(chan struct{})

	//The function starts by creating a new sarama.ProducerMessage object with the Topic and Value fields and stores it in the Kafka's queue.
	//It then increments the msgCount variable every time it sends a message into Kafka's queue, 
	//which is done until there are no more messages left in the queue or 5 seconds have passed without receiving any acknowledgements from Kafka on each message sent.
	//The function is waiting for signals from Kafka indicating that all messages were successfully received before continuing to send more messages into Kafka's queue.
	go func() {
		for {
			msgCount++

			msg := &sarama.ProducerMessage{
				Topic: topic,
				Value: sarama.StringEncoder(fmt.Sprintf("Hello Kafka %v", msgCount)),
			}

			partition, offset, err := producer.SendMessage(msg)
			if err != nil {
				panic(err)
			}

			fmt.Printf("Message is stored in topic(%s)/partition(%d)/offset(%d)\n", topic, partition, offset)
			time.Sleep(5 * time.Second)
		}
	}()

	<-doneCh
}

//The function starts by getting the broker address from the environment variable BROKER_ADDR.
//If it doesn't exist, then it will default to localhost:9092.
//The function returns the value of the environment variable.
func brokerAddr() string {
	brokerAddr := os.Getenv("BROKER_ADDR")
	if len(brokerAddr) == 0 {
		brokerAddr = "localhost:9092"
	}
	return brokerAddr
}

//The topic is also retrieved from the environment variable TOPIC and if it doesn't exist, then it will default to "default-topic".
//The function returns the value of the environment variable.
func topic() string {
	topic := os.Getenv("TOPIC")
	if len(topic) == 0 {
		topic = "default-topic"
	}
	return topic
}
