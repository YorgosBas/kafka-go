package main

import (
	"fmt"
	"os"
	"os/signal"
	"time"
	"github.com/Shopify/sarama"
)

func main() {
	fmt.Println("Starting Kafka subscriber...")

	//Consumer configuration
	config := sarama.NewConfig()
	config.Consumer.Offsets.Initial = sarama.OffsetOldest
	config.Consumer.Offsets.CommitInterval = 5 * time.Second
	config.Consumer.Return.Errors = true

	//Create new consumer for given brokers and configuration
	brokers := []string{brokerAddr()}
	master, err := sarama.NewConsumer(brokers, config)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := master.Close(); err != nil {
			panic(err)
		}
	}()

	//Decide about the offset here: literal value, sarama.OffsetOldest, sarama.OffsetNewest
	//This is important in case of reconnection
	consumer, err := master.ConsumePartition(topic(), 0, sarama.OffsetOldest)
	if err != nil {
		panic(err)
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	//Count how many messages are processed
	msgCount := 0
	
	//This is the channel that will be used to signal when the program has finished running
	doneCh := make(chan struct{})
	go func() {
		//The loop starts by checking for signals and handling them appropriately before exiting the loop with doneCh <- struct{}{}. 
		//This is where the code checks for errors, messages, and interrupts.
		//The code is meant to demonstrate how a consumer might handle the following: - Signals received from the producer - Errors encountered by the consumer
		//The first case of the select statement checks for errors, which are printed if they occur.
		//If there is no error, then it checks for messages on consumer's Messages() channel and increments msgCount accordingly.
		//Finally, signals are checked for and handled appropriately before exiting the loop with doneCh <- struct{}{}
		for {
			select {
			case err := <-consumer.Errors():
				fmt.Println(err)
			case msg := <-consumer.Messages():
				msgCount++
				fmt.Println("Received messages", string(msg.Key), string(msg.Value))
			case <-signals:
				fmt.Println("Interrupted")
				doneCh <- struct{}{}
			}
		}
	}()

	<-doneCh
	fmt.Println("Processed", msgCount, "messages")
}

//The function starts by getting the broker address from the environment variable BROKER_ADDR.
//If it doesn't exist, then it will default to localhost:9092.
//The function returns the value of the environment variable.
func brokerAddr() string {
	brokerAddr := os.Getenv("BROKER_ADDR")
	if len(brokerAddr) == 0 {
		brokerAddr = "localhost:9092"
	}
	return brokerAddr
}

//The topic is also retrieved from the environment variable TOPIC and if it doesn't exist, then it will default to "default-topic".
//The function returns the value of the environment variable.
func topic() string {
	topic := os.Getenv("TOPIC")
	if len(topic) == 0 {
		topic = "default-topic"
	}
	return topic
}
